function LP = LP(mu);
%Izracunamo polozaj Lagrangeove tocke in narisemo sistem treh teles
    
    %Izracunamo negirani mu
    nmu = 1-mu;
    
    %rezerviramo prostor za nase 
    LP = zeros(5,3);    

    %Izracunamo polozaj prve Lagrangeove tocke(L1)
    polinomL1 = [1, 2*(mu-nmu), nmu^2-4*nmu*mu+mu^2, 2*mu*nmu*(nmu-mu)+mu-nmu, mu^2*nmu^2+2*(nmu^2+mu^2), mu^3-nmu^3];
    L1roots = roots(polinomL1);
    L1 = 0;
    for(i = 1:5)
        if(L1roots(i) > -mu) && (L1roots(i) < nmu)
         L1 = L1roots(i);
        end
    end
    LP(1,1) = L1;
    
    %Izracunamo polozaj druge Lagrangeove tocke(L2)
    polinomL2 = [1, 2*(mu-nmu), nmu^2-4*nmu*mu+mu^2, 2*mu*nmu*(nmu-mu)-(mu+nmu), mu^2*nmu^2+2*(nmu^2-mu^2), -(mu^3+nmu^3)];
    L2roots = roots(polinomL2);
    L2 = 0;
    for(i = 1:5)
        if(L2roots(i) > -mu) && (L2roots(i) > nmu)
            L2 = L2roots(i);
        end
    end
    LP(2,1) = L2;

    
    %Izracunamo polozaj tretje Lagrangeove tocke(L3)
    polinomL3 = [1, 2*(mu-nmu), nmu^2-4*mu*nmu+mu^2, 2*mu*nmu*(nmu-mu)+(nmu+mu), mu^2*nmu^2+2*(mu^2-nmu^2), nmu^3+mu^3];
    L3roots = roots(polinomL3);
    L3 = 0;
    for(i = 1:5)
        if L3roots(i) < -mu
            L3 = L3roots(i);
        end
    end
    LP(3,1) = L3;

    
    %Izracunamo polozaj cetrte Lagrangeove tocke(L4)
    LP(4,1) = 0.5 - mu;
    LP(4,2) = sqrt(3)/2;


    %Izracunamo polozaj pete Lagrangeove tocke(L5)
    LP(5,1) = 0.5 - mu;
    LP(5,2) = -sqrt(3)/2;

    %Narisemo nas sistem
    hold on
    title("L tocke");
    xlabel("x-os");
    ylabel("y-os");
    zlabel("z-os");
    
    %Narisemo koordinati x in y
    plot3(linspace(-2,2,3),zeros(1,3),zeros(1,3),":","color",[1 0 1],"LineWidth",1);
    plot3(zeros(1,3),linspace(-2,2,3),zeros(1,3),":","color",[1 0 1]);
    
    %Narisemo telesi nasega sistema
    plot3(-mu,0,0,"*","LineWidth",10);
    plot3(nmu,0,0,"*","LineWidth",2.5);
    
    %Narisemo izhodisce(masno sredisce)
    plot3(0,0,0,'.',"color",[0 0 0]);
    
    %Narisemo stacinoarne tocke
    plot3(LP(:,1),LP(:,2),LP(:,3),"or");
    
    %Dodamo legendo
    legend("x","y","M1","M2","T","L");
    hold off

endfunction