function [t, Y] = satelit(svector, M, m, tmax)
%PRIMER UPORABE: [x,y] = satelit([0.9 0 0 0.3 -1 0.1],Mz,Ml,100000);

%[t, Y] = satelit(>0) zgenerira tirnice satelita v triseznem prostoru.
%Satelit se giblje po enacbah:
%
%   x. = a
%   y. = b
%   z. = c
%   a = x + 2*b - (1-mu)*(1/R^3)*(x + mu) - mu*(1/r^3)*(x - mu)
%   b = y - 2*a - (1-mu)*(1/R^3)*y - mu*(1/r^3)*y
%   c = -(1-mu)*(1/R^3)*z - mu*(1/r^3)*z 
%
%kjer je:
%   mu = oddaljenost telesa M od masnega sredisca
%   cmu = oddaljenost telesa m od masnega sredisca
%   R = oddaljenost satelita od M
%   r = oddaljenost satelita od m

%definiramo konstante in funkcije
mu = m / (M + m);
cmu = 1 - mu;

%zacetni polozaj in zacetna hitrost za Runge Kutta metodo
x0 = [svector(1:3)]';
v0 = [svector(4:6)]';

% ustvarimo vektor za zacetni pogoj (polozajni in hitrostni vektor)
Y0 = [x0; v0];
f = @f;
%dolzina koraka za Runge Kutta metodo
h = 0.0001;

%postavimo zacetne parametre... 
Y = Y0;
k = 1;
t = 0;
%... in resujemo z Runge Kutta metodo.
while(true)
    k1 = h*feval(f, t(k), Y(:, k), mu);
    k2 = h*feval(f, t(k) + h/2, Y(:, k) + k1/2, mu);
    k3 = h*feval(f, t(k) + h/2, Y(:, k) + k2/2, mu);
    k4 = h*feval(f, t(k) + h, Y(:, k) + k3, mu);
    Y(:, k+1) = Y(:, k) + (k1 + 2*k2 + 2*k3 + k4)/6;
    t(k+1) = t(k) + h;

    k = k + 1;
    if(k >= tmax)
        break
    end
end

%risanje poti satelita
plot3(Y'(:,1),Y'(:,2),Y'(:,3),"b","LineWidth",2);

endfunction

%Funkcija za izracun
function R = f(t,Y,mu)
       cmu = 1 - mu;
       R = sqrt((Y(1) + mu)^2 + Y(2)^2 + Y(3)^2);
       r = sqrt((Y(1) - cmu)^2 + Y(2)^2 + Y(3)^2);
        
     %Nekaj izracunov ki se ponavaljajo
     vmesni1 = cmu / (R^3);
     vmesni2 = mu / (r^3);
R = [Y(4:6);
     (Y(1) + (2*Y(5))) - ((vmesni1)*(Y(1) + mu)) - ((vmesni2)*(Y(1) - cmu));
     (Y(2) - (2*Y(4))) - ((vmesni1)*Y(2)) - ((vmesni2)*Y(2));
     -((vmesni1)*Y(3)) - ((vmesni2)*Y(3))];
endfunction
