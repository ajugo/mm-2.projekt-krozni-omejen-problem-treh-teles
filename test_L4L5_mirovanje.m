function test_L4L5_mirovanje()
%test_L4L5_mirovanje() shrani v mapo izvajanja serijo PNG
%graficnih datotek, ki prikazujejo simulacije satelitov v mirovanju,
%ko so postavljeni v tockah L4 in L5 za sisteme Sonce-Zemlja,
%Zemlja-Luna ter Sonce-Jupiter.

% Mase
Ms = 1.98855 * 10^30;
Mz = 5.97219 * 10^24;
Mj = 1.8986 * 10^27;
Ml = 7.3477 * 10^22;

% Sistemi
sistem1 = 'Sonce-Zemlja';
M = Ms;
m = Mz;
mu = m / (M + m);
Ltocke = intLP(mu);
Mvse = [M m mu];
Lvse = [Ltocke];

sistem2 = 'Zemlja-Luna';
M = Mz;
m = Ml;
mu = m / (M + m);
Ltocke = intLP(mu);
Mvse = [Mvse; [M m mu]];
Lvse = [Lvse; Ltocke];

sistem3 = 'Sonce-Jupiter';
M = Ms;
m = Mj;
mu = m / (M + m);
Ltocke = intLP(mu);
Mvse = [Mvse; [M m mu]];
Lvse = [Lvse; Ltocke];

% Stevilo korakov za metodo rk4
tmax = 10000;

for j=0:5:10
	for i=1:5
		% PLOT
		xlabel('x-os');
		ylabel('y-os');
		% stacionarne tocke
		plot3(Lvse(j+i, 1), Lvse(j+i, 2), Lvse(j+i, 3), 'or');
		hold on
		% satelit v L4 ali L5 z zacetno hitrostjo [0 0 0]
		[t, Y] = satelit([Lvse(j+i, :) 0 0 0], Mvse((j/5)+1, 1), Mvse((j/5)+1, 2), tmax);
		plot3(Y'(:,1), Y'(:,2), Y'(:,3), 'b', 'LineWidth', 2);
		view(2)

		if j == 0
			sistem = sistem1;
		elseif j == 5
			sistem = sistem2;
		else
			sistem = sistem3;
		end
		print(strcat(sistem, '-L', num2str(i), '.png'));
		hold off
	end
end

endfunction

function LP=intLP(mu);

    nmu = 1-mu;
    LP = zeros(5,3);

    %L1
    polinomL1 = [1, 2*(mu-nmu), nmu^2-4*nmu*mu+mu^2, 2*mu*nmu*(nmu-mu)+mu-nmu, mu^2*nmu^2+2*(nmu^2+mu^2), mu^3-nmu^3];
    L1roots = roots(polinomL1);
    L1 = 0;
    for(i = 1:5)
        if(L1roots(i) > -mu) && (L1roots(i) < nmu)
         L1 = L1roots(i);
        end
    end
    LP(1,1) = L1;
    
    %L2
    polinomL2 = [1, 2*(mu-nmu), nmu^2-4*nmu*mu+mu^2, 2*mu*nmu*(nmu-mu)-(mu+nmu), mu^2*nmu^2+2*(nmu^2-mu^2), -(mu^3+nmu^3)];
    L2roots = roots(polinomL2);
    L2 = 0;
    for(i = 1:5)
        if(L2roots(i) > -mu) && (L2roots(i) > nmu)
            L2 = L2roots(i);
        end
    end
    LP(2,1) = L2;

    
    %L3
    polinomL3 = [1, 2*(mu-nmu), nmu^2-4*mu*nmu+mu^2, 2*mu*nmu*(nmu-mu)+(nmu+mu), mu^2*nmu^2+2*(mu^2-nmu^2), nmu^3+mu^3];
    L3roots = roots(polinomL3);
    L3 = 0;
    for(i = 1:5)
        if L3roots(i) < -mu
            L3 = L3roots(i);
        end
    end
    LP(3,1) = L3;

    
    %L4
    LP(4,1) = 0.5 - mu;
    LP(4,2) = sqrt(3)/2;


    %L5
    LP(5,1) = 0.5 - mu;
    LP(5,2) = -sqrt(3)/2;

endfunction