function x = sekantna(X, F, M,m1,tol, maxit)
%x = sekantna(X, F, tol, maxit) poisce resitev
%enacbe F(x) = 0 z vecrazsezno sekantno metodo.

%primer g = @(alfa,M,m, tMax) (rk4Sekantna(alfa, M, m, tMax) - [0.990045;0;-0.00012231887]);
%       T = [0.99004500001 0 -0.0001223188700001;
%              0.9900450001 0 -0.000122318870001;
%              0.990045001 0 -0.00012231887001;
%             0.990045 0 -0.00012231887]';

%        T = [0.990045 0 -0.00012094;
%             0.990045 0 -0.00012104;
%             0.990045 0 -0.00012154;
%             0.990045 0 -0.00012204]'

%        sekantna(T, g, 10^-10, 100, Ms, Mz);       


%dobimo velikost matrike
[n,m] = size(X);
%pripravimo 1. bazni vektor R^(n+1)
e1 = [1; zeros(n,1)];

%Pred zacetkom iteracije pripravimo Z
for j = 1:m
  Z(:,j) = [1; feval(F, X(:, j)',M,m1, 80000)];
end

for k = 1:maxit
  x = X*(Z\e1); %nov priblizek
  Fx = feval(F, x, M,m1, 100000);
  if(norm(Fx) < tol)
    break;
  end
  X = [X(:,2:end), x];
  Z = [Z(:, 2:end),[1;Fx]];
end