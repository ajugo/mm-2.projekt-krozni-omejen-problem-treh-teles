function animiran(x, y, z, s)
	% x, y, z - stolpicni vektorji s koordinatami tira
	% ie. kar satelit.m vrze ven v drugi argument, transponirano
	% s - za pospesitev risanja tira
	% majhen s je bolj natancen, velik s izrise tir hitreje

	% primer uporabe:
	% [x,y] = satelit([0.9 0 0 0.3 -1 0.1],Mz,Ml,1000);
	% animiran(y'(:,1), y'(:,2), y'(:,3), 100)
	
	% spodnje poveca graf da se ne povecuje med izrisom
	% za uporabo odkomentiraj tudi axis([-c, c, -c, c]);

	%a = abs(min(min(min(x), min(y)), -2));
	%b = abs(max(max(max(x), max(x)), 2));
	%c = max(abs(a), abs(b))

	p = plot3(x(1),y(1),z(1),"r","LineWidth",3);
	%axis([-c, c, -c, c]);
	view(2);

	for i = 1:s:length(x);
	   set(p, 'XData', x(1:i));
	   set(p, 'YData', y(1:i));
	   set(p, 'ZData', z(1:i));
	   pause (0.001); 
	endfor

