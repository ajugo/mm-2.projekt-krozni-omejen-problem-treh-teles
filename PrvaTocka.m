1;
%Skripta izrise sisteme Sonce-Zemlja, Zemlja-Luna, Sonce-Jupiter ter
%vse njihove pripadajoce Lagrangeove tocke

%Masa Zemlje 
Mz = 5.97219 * 10^24;
%Masa Lune
Ml = 7.3477 * 10^22;
%Masa Sonca
Ms = 1.98855 * 10^30;
%Masa Jupitra
Mj = 1.898 * 10^27;
 
%Izracunamo mu za sistem Sonce-Zemlja 
mu1 = Mz/(Ms+Mz);

%Izracunamo mu za sistem Zemlja-Luna
mu2 = Ml/(Ml+Mz);

%Izracunamo mu za sistem Sonce-Jupiter
mu3 = Mj/(Ms+Mj);

%Izrisemo sistem Sonce-Zemlja
figure(1);
LPsz = LP(mu1);
%Izrisemo sistem Zemlja-Luna
figure(2);
LPzl = LP(mu2);
%Izrisemo sistem Sonce-Jupiter
figure(3);
LPsj = LP(mu3);